<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of newPHPClass
 *
 * @author JPiagetti
 */
class Produtos extends CI_Controller {

    public function index() {
        $this->load->view('template/header.php');
        $this->load->view('index.php');
        $this->load->view('template/footer.php');
    }

    public function Exibe() {
        $dados['product'] = $this->Produtos_model->buscarTodos();
        $this->load->view('produtosView', $dados);
    }

    public function Inserir() {
        $this->load->view('cadastroProdutos');
        $products = array('nomeproduto' => $this->input->post('nome'),
            'descricao' => $this->input->post('desc'),
            'preco' => $this->input->post('preco'),
            'categoria' => $this->input->post('categoria'));
        $this->Produtos_model->inserir($products);
    }

    public function Excluir() {
        $dados['product'] = $this->Produtos_model->buscarTodos();
        $this->load->view('excluirView', $dados);
        $product = $this->input->post('idproduto');
        $this->Produtos_model->Excluir($product);
    }

    public function Atualizar() {
         $dados['product'] = $this->Produtos_model->buscarTodos();
         $id=
        $this->Produtos_model->atualizar();
        $this->load->view('atualizarView',$dados);
    }

}
