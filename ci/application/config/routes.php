<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'Produtos';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['inserir'] = 'Produtos/inserir';
$route['exibe'] = 'index.php/Produtos/exibeProdutos';
$route['atualizar'] = 'Produtos/atualizar';
$route['editar/(:num)'] = 'Produtos/editar/$1';
$route['excluir/(:num)'] = 'Produtos/excluir/$1';
