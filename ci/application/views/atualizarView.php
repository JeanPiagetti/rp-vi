<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <h1 class="title"><i class="fa fa-gears fa-2x"></i> Produtos </h1>
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Produto</th>
                    <th>Descrição</th>
                    <th>Categoria</th>
                    <th>Preço</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <?php foreach ($product as $produtos): ?>
                        <td><?php echo $produtos->nomeproduto; ?></td>
                        <td><?php echo $produtos->descricao; ?></td>
                        <td><?php echo $produtos->categoria; ?></td>
                        <td><?php echo $produtos->preco; ?></td>
                        <td><a href="#box-editar" data-toggle="modal" data-target="#box-editar"><i class="fa fa-pencil "></i> Editar</a></td>
                    </tr>

                <?php endforeach; ?>
            </tbody>
        </table><br />

        <!-- Modal editar -->
        <div class="modal fade" id="box-editar" tabindex="-1" role="dialog" aria-labelledby="boxLabel" aria-hidden="true">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="boxLabel"><i class="fa fa-pencil "></i> Editar Produto</h4>
                    </div>
                    <div <form role="form" method="post" action="user/edicao.php">
                            <div class="form-group">
                                <input id="id" class="form-control"  name="id" type="hidden">
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="form-group">
                                        <?php
                                        echo form_open('Produtos/Atualizar');
                                        echo form_label('Nome do produto');
                                        echo form_input(array('name' => 'nomeproduto', 'value' => $produtos->nomeproduto));
                                        echo form_label('Descrição');
                                        echo form_input(array('name' => 'descricao', 'value' => $produtos->descricao));
                                        echo form_label('Preço');
                                        echo form_input(array('name' => 'preco', 'value' => $produtos->preco));
                                        echo form_label('Categoria');
                                        echo form_input(array('name' => 'categoria', 'value' => $produtos->categoria));
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <?php
                                echo form_button(array('type' => 'submit',
                                    'class' => 'btn btn-primary', 'content' => 'Salvar alterações'));
                                echo form_close();
                                ?>
                            </div>
                    </div>
                </div>
            </div>
<!--            <script>
                $('#idDoModalContent').load('localhost/ci/index.php/index.php/Produtos/Atualizar', function (result) {
                    $('#idDoModalContent').html(result);
                });
            </script>-->
    </body>
</html>
