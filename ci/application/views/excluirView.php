<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <meta charset="UTF-8">
        <title>Excluir</title>
    </head>
    <body>
        <table class="table" >
            <tr>
                <th>Id</th>
                <th>Nome</th>
                <th>Descricao</th>
                <th>Preço</th>
                <th>Categoria</th>
            </tr>
            <?php foreach ($product as $produtos): ?>
                <tr>
                    <td><?php echo form_label($produtos->idproduto) ?></td>
                    <td><?php echo form_label($produtos->nomeproduto); ?></td>
                    <td><?php echo form_label($produtos->descricao); ?></td>
                    <td><?php echo form_label('R$' . $produtos->preco); ?></td>
                    <td><?php echo form_label($produtos->categoria); ?></td>
                </tr>
            <?php endforeach; ?>
        </table>

        <div class="form-group">
            <?php
            echo form_open('Produtos/excluir');
            echo form_label('Digite id do produto para excluir');
            echo form_input(array('name' => 'idproduto', 'class' => 'form-control', 'maxlenght' => '255'));
            echo form_button(array('type' => 'submit',
                'class' => 'btn btn-primary', 'content' => 'Excluir'));
            echo form_close();
            ?>
        </div>
    </body>
</html>
