<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Produtos_model
 *
 * @author JPiagetti
 */
class Produtos_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function buscarTodos() {
        return $this->db->get('produto')->result();
    }

    public function Inserir($products) {
        if (!isset($products)) {
            return false;
        }
        return $this->db->insert('produto', $products);
    }

    public function Excluir($id) {
        $this->db->where('idproduto', $id);
        return $this->db->delete('produto');
    }

    public function Atualizar($id, $dados) {
       
        $this->db->where('id', $id);
        return $this->db->update('produto', $dados);
    }

}
